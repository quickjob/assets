<?php namespace Quickjob\Assets;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class AssetsServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = true;

	public function boot() {
		$this->package('quickjob/assets');
		// $this->app['quickjob.templates']->registerTemplateDirectory(__DIR__ . '/templates','assets');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app['quickjob.assets'] = $this->app->share(function($app)
		{
			return new Environment($app);
		});

		$this->app->booting(function()
		{
			$this->app['view']->addNamespace('assets',__DIR__ . '/templates');
			$loader = AliasLoader::getInstance();
			$loader->alias('Assets', 'Quickjob\Assets\Facades\Assets');
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('quickjob.assets');
	}

}
