<?php namespace Quickjob\Assets;

class Environment {

  protected $app = null;

  protected $containers = array();

  public function __construct($app)
  {
    $this->app = $app;
  }

  public function container($container = 'default')
  {
    if ( ! isset($this->containers[$container]))
    {
      $this->containers[$container] = new Container($this->app, $container);
    }

    return $this->containers[$container];
  }

  public function clearAll() {
  	foreach ($this->containers as $container) {
  		$container->clear();
  	}
  }

  public function __call($method, $parameters)
  {
    return call_user_func_array(array($this->container(), $method), $parameters);
  }
}
