<?php namespace Quickjob\Assets;

use Quickjob\Loader\Facades\Quickjob;

class FileNotExistsException extends \Exception {}

class Asset {

	public function __construct($app, $type, $assetData, $conditional, $isUrl) {
		// $this->templateEngine = $app['quickjob.templates'];
		$this->app = $app;
		$this->type = $type;
		$this->data = $assetData;
		$this->url = $isUrl;
		$this->conditional = $conditional;
	}


	protected function getAttr() {
		$assetData = $this->data;
		$attributes = $assetData['attributes'];

		if(!is_array($attributes)) {
			$attributes = array();
		}

		if($this->type === 'style') {

			if ( ! array_key_exists('media', $attributes)) {
				$attributes['media'] = 'all';
			}
		}
		return $attributes;
	}

	public function render() {
		$assetData = $this->data;
		$attributes = $this->getAttr();

		$html = '';
		$afterHtml = "\n";

		if($this->conditional !== false) {
			$html .= '<!--[if '.$this->conditional.']>'."\n";
			$afterHtml = "\n".'<![endif]-->'.$afterHtml;
		}

		if(!empty($assetData['source'])) {
			if (!$this->isUrl($assetData['source'])){
				$file = $assetData['source'];
				$assetData['source'] = $this->getUrlWithMtime($assetData['source']);
				if($assetData['source'] === false) {
					$assetData['type'] = 'script';
					$assetData['source'] = null;
					$assetData['data'] = 'console.log("%cAsset file '.$file.' does not exist","color: #d00;");';
				}
			}

			$attr = call_user_func_array(array($this->app['html'], 'attributes'), array(
				$attributes,
				));

			$html .= $this->app['view']->make('assets::'.$this->type,array(
				'src' => $assetData['source'],
				'attr' => $attr,
			));
		}

		if(!empty($assetData['data'])) {
			$attr = call_user_func_array(array($this->app['html'], 'attributes'), array(
				$assetData['attributes']
				));
			$html .= '<script '.$attr.">//<![CDATA[ \n".$assetData['data']."    //]]>\n</script>";
		}

		$html .= $afterHtml;

		return $html;
	}

	public function getSource() {
		$data = $this->data;
		return empty($data['source']) ? $data['data'] : $data['source'];
	}

	public function getConditional() {
		return $this->conditional;
	}

	protected function getUrlWithMtime($url) {
		$path = str_replace(url(''), '', $url);

		$file = $this->app['path.public'].'/'.ltrim($path, '/');
		if(!is_file($file)) {
			return false;
			// throw new FileNotExistsException("Asset file {$file} does not exist");
		}
		$modified = filemtime($file);

		return $url . "?{$modified}";
	}

	protected function isUrl($path) {
		$url = url('');
		if($this->url) {
			return true;
		}
		if(strpos($path, $url) !== false) {
			return false;
		}
		return filter_var($path, FILTER_VALIDATE_URL) === true || strpos($path, '//') > -1;
	}

	public function toArray() {
		$assetData = $this->data;
		$attr = $this->getAttr();

		if(empty($assetData['source']) || $this->conditional !== false)
			return;

		if (!$this->isUrl($assetData['source'])){
			$assetData['source'] = $this->getUrlWithMtime($assetData['source']);
		}
		$data = array(
			'href' => $assetData['source'],
		);
		if($this->type === 'style') {
			$data['rel']   = isset($attr['rel']) ? $attr['rel'] : 'stylesheet';
			$data['media'] = $attr['media'];
		}
		return $data;
	}
}
