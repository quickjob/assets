<?php namespace Quickjob\Assets;

namespace Quickjob\Assets;

use RuntimeException;

class InvalidMethodException extends \Exception {}
class InvalidAssetException extends \Exception {}
class InvalidDataException extends \Exception {}

class Container {

	protected $app = null;

	protected $name = null;

	protected $assets = array(
		'script' => array(),
		'style' => array(),
	);

	protected $isConditional = false;
	protected $isUrl = false;

	protected $allowedAssets = array(
		'',
		'script',
		'style',
		'data',
		'inlinescript'
	);

	public function __construct($app, $name)
	{
		$this->app  = $app;
	}

	public function conditional($condition) {
		$this->isConditional = $condition;
		return $this;
	}

	public function url() {
		$this->isUrl = true;
		return $this;
	}

	public function clear() {
		$this->assets = array(
			'script' => array(),
			'style' => array(),
		);
	}

	public function __call($called,$args) {
		$method = null;

		if(!method_exists($this, $method)) {

			if(array_search(strtolower($called), $this->allowedAssets)) {
				$operation = 'push';
				$type = strtolower($called);
			} else {
				$m = preg_split('/(?=[A-Z])/',$called);
				$method = $m[0];
				unset($m[0]);

				$type = !isset($m[1]) ? null : strtolower(implode('', $m));
			}

			$source = !isset($args[0]) ? null : $args[0];
			$attr = !isset($args[1]) ? null : $args[1];

			if(!empty($type) && !array_search($type, $this->allowedAssets))
				return false;
			if(empty($operation)) {
				if($method === 'append') {
					$operation = 'push';
				} elseif($method === 'prepend') {
					$operation = 'unshift';
				} else {
					throw new \Exception("Method $method does not exist in the Assets class.");
				}
			}


			if (empty($type)) {
				$type = $this->_getFormat($args[0]);
			}

			if($type === 'data') {
				if(!isset($args[1])) {
					throw new \Exception("Error Processing Request");
				}

				$data = $args[1];
				$attr = !isset($args[3]) ? null : $args[3];

				$isVar = strpos($source, '.') === false ? 'var ' : '';
				$script = "$isVar{$source} = ".json_encode($data).";";

				$source = $script;
			}


			if(!$this->isRegistered($source)) {
				$this->_addAsset($type,$source,$attr,$this->isConditional,$operation);
			}

			$this->isConditional = false;
			$this->isUrl = false;
			return $this;
		}

	}

	protected function _getFormat($source) {
		if(strpos($source, '.css') === false && strpos($source, '.js') === false) {
			throw new \Exception("Could not define the type of the asset {$source}.", 1);
		}
		return (pathinfo($source, PATHINFO_EXTENSION) == 'css') ? 'style' : 'script';
	}

	protected function _addAsset($type,$source,$attr,$conditional,$method) {
		$assetData = array(
			'source' => $source,
			'attributes' => $attr
			);
		if($type === 'data' || $type === 'inlinescript') {
			$type = 'script';
			$assetData['source'] = null;

			$assetData['data'] = $source;
		}

		$asset = new Asset($this->app, $type, $assetData, $this->isConditional, $this->isUrl);

		$method = 'array_'.$method;
		$method($this->assets[$type],$asset);
	}

	public function styles()
	{
		return $this->group('style');
	}

	public function scripts()
	{
		return $this->group('script');
	}

	protected function group($group)
	{
		if ( ! isset($this->assets[$group]) or count($this->assets[$group]) === 0) return '';

		$assets = '';
		foreach ($this->assets[$group] as $asset) {
			$assets .= $asset->render();
		}

		return $assets;
	}

	protected function isRegistered($source)
	{
		if($this->isConditional) {
			return false;
		}

		foreach($this->assets as $arr) {
			foreach($arr as $asset) {
				if($asset->getSource() === $source) {
					return true;
				}
			}
		}
		return false;
	}
	protected function isUrl($path)
	{
		return filter_var($path, FILTER_VALIDATE_URL) === true || strpos($path, '//') > -1;
	}
	public function stylesArray() {
		$styles = array();
		foreach($this->assets['style'] as $s) {
			$data = $s->toArray();
			if(!empty($data['href']))
				$scripts[] = $data;
		}
		return $styles;
	}
	public function scriptsArray() {
		$scripts = array();
		foreach($this->assets['script'] as $s) {
			$data = $s->toArray();
			if(!empty($data['href']))
				$scripts[] = $data;
		}
		return $scripts;
	}
}
